﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CaveSetupComponent.h"
#include "LiveLinkPreset.h"
#include "Logging/StructuredLog.h"
#include "Utility/RWTHVRClusterUtilities.h"

UCaveSetupComponent::UCaveSetupComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	SetIsReplicatedByDefault(false);
}

void UCaveSetupComponent::BeginPlay()
{
	Super::BeginPlay();


	if (!URWTHVRClusterUtilities::IsRoomMountedMode())
	{
		UE_LOGFMT(RWTHVRCluster, Display, "CaveSetup: Not in RoomMountedMode, doing nothing.");
		return;
	}

	// Spawn all actors that are set in the blueprint asset.
	for (const auto ActorClass : ActorsToSpawnOnCave)
	{
		if (const auto World = GetWorld())
		{
			const auto Actor = World->SpawnActor(ActorClass);
			Actor->AttachToActor(GetOwner(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);
			UE_LOGFMT(RWTHVRCluster, Display, "CaveSetup: Spawned Actor {Actor} on the Cave and attached it.",
					  Actor->GetName());
		}
	}

	// Apply the DTrack LiveLink Preset. Only do this if we are the primaryNode

	if (URWTHVRClusterUtilities::IsPrimaryNode())
	{
		if (LiveLinkPresetToApplyOnCave && LiveLinkPresetToApplyOnCave->IsValidLowLevelFast())
		{
			LiveLinkPresetToApplyOnCave->ApplyToClientLatent();

			UE_LOGFMT(RWTHVRCluster, Display, "CaveSetup: Applied LiveLinkPreset {Preset} to Client.",
					  LiveLinkPresetToApplyOnCave->GetName());
		}
	}
}
