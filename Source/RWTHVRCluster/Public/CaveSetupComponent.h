﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "CaveSetupComponent.generated.h"

class ULiveLinkPreset;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent),
	   hideCategories = (Rendering, Input, Actor, Base, Collision, Shape, Physics, HLOD))
class RWTHVRCLUSTER_API UCaveSetupComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UCaveSetupComponent();

	UPROPERTY(EditAnywhere)
	TArray<UClass*> ActorsToSpawnOnCave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ULiveLinkPreset* LiveLinkPresetToApplyOnCave;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
};
