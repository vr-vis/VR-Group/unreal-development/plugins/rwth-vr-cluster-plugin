#pragma once

#include "CoreMinimal.h"
#include "Components/DisplayClusterCameraComponent.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "UObject/ConstructorHelpers.h"

#include "RWTHVRClusterUtilities.generated.h"


/**
 * Custom log category for all ClusterPlugin related components
 */
DECLARE_LOG_CATEGORY_EXTERN(RWTHVRCluster, Log, All);

UENUM(BlueprintType)
enum class ENamedClusterComponent : uint8
{
	/* CAVE Specific */
	NCC_CAVE_ORIGIN UMETA(DisplayName = "CAVE Origin"),
	NCC_CAVE_CENTER UMETA(DisplayName = "CAVE Center"),
	NCC_CAVE_LHT UMETA(DisplayName = "CAVE Left Hand Target"),
	NCC_CAVE_RHT UMETA(DisplayName = "CAVE Right Hand Target"),

	/* ROLV Specific */
	NCC_ROLV_ORIGIN UMETA(DisplayName = "ROLV Origin"),

	/* Non Specific */
	NCC_CALIBRATIO UMETA(DisplayName = "Calibratio Motion to Photon Measurement Device"),
	NCC_SHUTTERGLASSES UMETA(DisplayName = "CAVE/ROLV/TDW Shutter Glasses"),
	NCC_FLYSTICK UMETA(DisplayName = "CAVE/ROLV/TDW Flystick"),
	NCC_TRACKING_ORIGIN UMETA(DisplayName = "CAVE/ROLV/TDW Origin")
};

UCLASS()
class RWTHVRCLUSTER_API URWTHVRClusterUtilities : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category = "RWTHVRCluster|Platform")
	static bool IsRoomMountedMode();
	UFUNCTION(BlueprintPure, Category = "RWTHVRCluster|Platform")
	static bool IsCave();
	UFUNCTION(BlueprintPure, Category = "RWTHVRCluster|Platform")
	static bool IsRolv();

	UFUNCTION(BlueprintPure, Category = "RWTHVRCluster")
	static bool IsPrimaryNode();
	UFUNCTION(BlueprintPure, Category = "RWTHVRCluster")
	static bool IsSecondaryNode();

	UFUNCTION(BlueprintPure, Category = "RWTHVRCluster")
	static FString GetNodeName();
	/* Distance in meters */
	UFUNCTION(BlueprintPure, Category = "RWTHVRCluster")
	static float GetEyeDistance();

	UFUNCTION(BlueprintPure, Category = "RWTHVRCluster")
	static EDisplayClusterEyeStereoOffset GetNodeEyeType();
};
